#include <ESP8266Ping.h>
#include <ESP8266WiFi.h>

//ssid credentials
const char *SERVER_WIFI_SSID = "MACLAN3";
const char *SERVER_WIFI_PASS = "mackrackit33";
const char* remote_host = "www.google.com";

void WiFi_Connect()
{
   Serial.print("Connecting to WiFi ");
   WiFi.begin(SERVER_WIFI_SSID,SERVER_WIFI_PASS);
   while(WiFi.status() != WL_CONNECTED)
   {
     delay(500);
     Serial.print(".");
   }

   Serial.println("Connected");
   Serial.println(WiFi.localIP());
}

void setup() {
  Serial.begin(9600);
  pinMode(0, OUTPUT);
  digitalWrite(0, LOW);//sets LED to ON, active low pin.
  WiFi_Connect();
}

void loop() {
  
  if(Ping.ping(remote_host)){
    Serial.println("Success!!");
    delay(900000); //sleeps 15 minutes (900000)
  }
  else {
    Serial.println("REBOOTING ROUTER");
    digitalWrite(0, HIGH);
    delay(30000); //5 seconds for testing, 30 in production.
    // the delay of 30 seconds allows any charge to drain in the router.
    digitalWrite(0, LOW);
    //a delay of 2 minutes gives the router a chance to fully boot before attempting
    // to reconnect again.
    delay(120000);
    //ESP.restart();
    WiFi_Connect();
    loop();
    
  }
  
}
