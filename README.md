#ESP8266_Ping
This project uses an esp8266-01S module to ping a remote host (google.com). If no response is received, then the module will trip a relay on GPIO_00. A router will be connected to the relay. If the internet goes down, for whatever, reason then the relay will be switched off and on again, rebooting the router. This will hopefully fix what is causing the internet outage, which we assume is the router just needing a reboot. 

###Flashing Instructions
This project uses the ESP8266WiFi library as well as the ESP8266Ping library. The zip directory containing the current, at time of this write up, clone from their github.

To install the ESP8266 library and subsequent ESP8266WiFi library that comes packaged with it, you will need to preform the following steps.

* Open Arduino
* Go to File then Preferences
* Under 'Additional Boards Manager URLs' copy the following link
* http://arduino.esp8266.com/stable/package_esp8266com_index.json
* Click OK
* Go to Tools/Board:/Board Manager
* Search for esp8266, only one entry should show up, the one you just made arduino aware of ealier with the link.
* Tell Arduino to install
* Reboot Arduino for good measure
* It will now show up under Tools/Boards as Generic ESP8266
* You will now have the ESP8266WiFi library installed as well

To install the ESP8266Ping Library from the zip included simply do as follows.

* With arduino open, go to Sketch/Include Library/ Add .ZIP Library
* Find and Select the file on your machine, hit OK
* Alternately, you could download the current zip file from the projects github repo. However, it can not be guaranteed that it will perform the same. Use at your own discrection.
* Here is the link, https://github.com/dancol90/ESP8266Ping

###Other Notes
For this project I have exclusively used the esp8266-01S. That is with an "S". I have tried to use the esp8266-01, but got sporadic results. If you want to be sure yours works as expected, please use the esp8266-01S. The S model has double the memory, at 1MB, and increased wifi performance, so there is no reason to not use it.

